<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Awb;
use App\Models\Transaction;
use App\Models\TransactionData;
use App\Models\TransactionNote;
use App\Models\Connote;
use App\Models\Customer;
use App\Models\Koli;
class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactions = Transaction::join('m_transaction_data', 'mt_id', 'mtd_mt_id')
                                   ->join('m_customer', 'mtd_owner', 'mc_id')
                                   ->select('mt_id as transaction_id', 'mc_name as customer_name', 'mt_amount as transaction_amount')
                                   ->get();
        return $transactions;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'connote_id' => 'required',
            'koli_chargeable_weight' => 'required|min:0|max:50',
            'koli_width' => 'required|numeric',
            'koli_height' => 'required|numeric',
            'koli_description' => 'required',
            'koli_volume' => 'required|numeric',
            'koli_weight' => 'required|numeric',
        ]);
        $connote = Connote::find($request->post('connote_id'));
        $koli_id = $this->generate_id();
        if($connote){
            $connoteCount = Connote::join('m_koli', 'mco_id', 'mk_mco_id')
                                   ->count();
            $connoteCount =  intval($connoteCount)+1;
            $insert_koli = array(
                'mk_id' => $koli_id,
                'mk_formula_id' => 0,
                'mk_mco_id' => $request->post('connote_id'),
                'mk_length' => $request->post('koli_length'),
                'mk_width' => $request->post('koli_width'),
                'mk_height' => $request->post('koli_height'),
                'mk_description' => $request->post('koli_description'),
                'mk_volume' => $request->post('koli_volume'),
                'mk_weight' => $request->post('koli_weight'),
                'mk_chargeable_weight' => $request->post('koli_chargeable_weight'),
                'mk_code' => $connote->mco_code.'.'.$connoteCount,
            );
            $insert_awb = array(
                'ma_mk_id' => $koli_id,
                'ma_url' => 'https:\/\/tracking.mile.app\/label\/'.$connote->mco_code.'.'.$connoteCount,
                'ma_price' => $request->post('harga_barang'),
                'ma_sicepat' => $request->post('awb_sicepat')
            );
            Koli::create($insert_koli);  
            Awb::create($insert_awb);
            return response()->json($insert_koli, 201);
        }else{
            abort(404);
        }
    }

    private function generate_id(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $first_random = '';
        $second_random = '';
        $third_random = '';
        $four_random = '';
        $five_random = '';
        for ($i = 0; $i < 8; $i++) {
            $first_random .= $characters[rand(0, $charactersLength - 1)];
        }
        for ($i = 0; $i < 4; $i++) {
            $second_random .= $characters[rand(0, $charactersLength - 1)];
            $third_random .= $characters[rand(0, $charactersLength - 1)];
            $four_random .= $characters[rand(0, $charactersLength - 1)];
        }
        for ($i = 0; $i < 12; $i++) {
            $five_random .= $characters[rand(0, $charactersLength - 1)];
        }
        return $first_random.'-'.$second_random.'-'.$third_random.'-'.$four_random.'-'.$five_random;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);
        if($transaction){
            $transaction_detail = Transaction::join('m_transaction_data', 'mt_id', 'mtd_mt_id')
                                              ->join('m_customer as owner', 'mtd_owner', 'owner.mc_id')
                                              ->join('m_customer as origin', 'mtd_origin', 'origin.mc_id')
                                              ->join('m_customer as destination', 'mtd_destination', 'destination.mc_id')
                                              ->join('m_location', 'mt_loc_id', 'ml_id')
                                              ->join('m_state', 'mt_state', 'mst_id')
                                              ->join('m_connote', 'mt_id', 'mco_mt_id')
                                            ->select('mt_id as transaction_id', 'owner.mc_name as customer_name', 'owner.mc_code as customer_code', 'mt_amount as transaction_amount', 'mt_discount as transaction_discount', 'mt_additional_field as transaction_additional_field', 'mst_name as transaction_state','mt_payment_id as transaction_payment_type', 'mst_name as transaction_state', 'mt_code as transaction_code', 'mt_order as transaction_order', 'ml_id as location_id', 'mt_organization_id as organization_id', 'm_transaction.created_at as created_at', 'm_transaction.updated_at as updated_at', 'mt_payment_type_name as transaction_payment_type_name', 'mt_cash_amount as transaction_cash_amount', 'mt_cash_change as transaction_cash_change', 'mco_id as connote_id')
                                            ->first();
            $connote = Connote::join('m_state', 'mco_state_id', 'mst_id')
                              ->join('m_zone as from', 'mco_zone_from', 'from.mz_id')
                              ->join('m_zone as to', 'mco_zone_from', 'to.mz_id')
                              ->join('m_location', 'mco_ml_id', 'ml_id')
                              ->join('m_source_tarif', 'mco_msta_id', 'msta_id')
                              ->where('mco_id', $transaction_detail->connote_id)
                              ->select('mco_id as connote_id', 'mco_number as connote_number', 'mco_service as connote_service', 'mco_service_price as connote_service_price', 'mco_amount as connote_amount', 'mco_code as connote_code', 'mco_booking_code as connote_booking_code', 'mco_order as connote_order', 'mst_name as connote_state', 'mst_id as connote_state_id', 'from.mz_code as zone_code_from', 'from.mz_code as zone_code_to', 'mco_surcharge_amount as surcharge_amount', 'mco_mt_id as transaction_id', 'mco_actual_weight as actual_weight', 'mco_volume_weight as volume_weight', 'mco_chargeable_weight as chargeable_weight', 'm_connote.created_at as created_at', 'm_connote.updated_at as updated_at', 'mco_organization_id as organization_id', 'mco_ml_id as location_id', 'mco_total_package as connote_total_package', 'mco_surcharge_amount as connote_surcharge_amount', 'mco_sla_day as connote_sla_day', 'ml_name as location_name', 'ml_type as location_type', 'msta_name as source_tariff_db', 'mco_msta_id as source_tariff_db', 'mco_pod as pod')
                              ->first();
            $origin_data = TransactionData::join('m_customer', 'mtd_origin', 'mc_id')
                                          ->join('m_zone', 'mc_zone_id', 'mz_id')
                                          ->where('mtd_mt_id', $transaction_detail->transaction_id)
                                          ->select('mc_name as customer_name', 'mc_address as customer_address', 'mc_email as customer_email', 'mc_phone as customer_phone', 'mc_address_detail as customer_address_detail', 'mc_zip_code as customer_zip_code', 'mz_code as zone_code', 'mc_organization_id as organization_id', 'mc_loc_id as location_id')
                                          ->first();
            $destination_data = TransactionData::join('m_customer', 'mtd_destination', 'mc_id')
                                              ->join('m_zone', 'mc_zone_id', 'mz_id')
                                              ->where('mtd_mt_id', $transaction_detail->transaction_id)
                                              ->select('mc_name as customer_name', 'mc_address as customer_address', 'mc_email as customer_email', 'mc_phone as customer_phone', 'mc_address_detail as customer_address_detail', 'mc_zip_code as customer_zip_code', 'mz_code as zone_code', 'mc_organization_id as organization_id', 'mc_loc_id as location_id')
                                              ->first();
            $destination_data = TransactionData::join('m_customer', 'mtd_destination', 'mc_id')
                                              ->join('m_zone', 'mc_zone_id', 'mz_id')
                                              ->where('mtd_mt_id', $transaction_detail->transaction_id)
                                              ->select('mc_name as customer_name', 'mc_address as customer_address', 'mc_email as customer_email', 'mc_phone as customer_phone', 'mc_address_detail as customer_address_detail', 'mc_zip_code as customer_zip_code', 'mz_code as zone_code', 'mc_organization_id as organization_id', 'mc_loc_id as location_id')
                                              ->first();
            $koli = Connote::join('m_koli', 'mco_id', 'mk_mco_id')
                           ->join('m_awb', 'mk_id', 'ma_mk_id')
                           ->where('mco_id', $connote->connote_id)
                           ->select('mk_length as koli_length', 'ma_url as awb_url', 'm_koli.created_at as created_at', 'mk_chargeable_weight as koli_chargeable_weight', 'mk_width as koli_width', 'mk_height as koli_height', 'm_koli.updated_at as updated_at', 'mk_description as koli_description', 'mk_formula_id as koli_formula_id', 'mk_mco_id as connote_id', 'mk_volume as koli_volume', 'mk_weight as koli_weight', 'mk_id as koli_id', 'mk_code as koli_code')
                           ->get();
            foreach($koli as $key => $kl){
                $custome_field = Awb::where('ma_mk_id', $kl->koli_Id)
                                    ->select('ma_sicepat as awb_sicepat', 'ma_price as harga_barang')
                                    ->first();
                $koli[$key]->koli_costum_field = $custome_field;
            }
            $transaction_detail->connote = $connote;
            $transaction_detail->origin_data = $origin_data;
            $transaction_detail->destination_data = $destination_data;
            $transaction_detail->koli_data = $koli;
            $transaction_detail->custom_field = TransactionNote::where('mtn_mt_id', $transaction_detail->transaction_id)
                                                               ->select('mtn_description as catatan_tambahan')
                                                               ->first();
            $transaction_detail->currentLocation = Transaction::join('m_location', 'mt_current_location', 'ml_id')
                                                              ->where('mt_id', $transaction_detail->transaction_id)
                                                              ->select('ml_name as name', 'ml_code as code', 'ml_type as type')
                                                              ->first();

            return $transaction_detail;
        }else{
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'mk_chargeable_weight' => 'required|min:0|max:50',
            'mk_width' => 'required|numeric',
            'mk_height' => 'required|numeric',
            'mk_description' => 'required',
            'mk_volume' => 'required|numeric',
            'mk_weight' => 'required|numeric',
        ]);
        $package = Koli::find($id);
        if(!$package){
            abort(404);
        }
        $package->update($request->all());
        return response()->json($package, 200);
    }

    public function updateAttribute(Request $request, $id)
    {
        $this->validate($request, [
            'mk_description' => 'required'
        ]);
        $package = Koli::find($id);
        if(!$package){
            abort(404);
        }
        $update_koli = array(
                'mk_description' => $request->post('koli_description')
            );
        $package->update($request->all());
        return response()->json($package, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $package = Koli::find($id);
        if($package == null){
            abort(404);
        }else{
            dd('awdawda');
        }
        Koli::find($id)->delete();
        Awb::where('ma_mk_id',$id)->delete();
        return response()->json(array("message" => 'Deleted Successfully'), 200);
    }
}
