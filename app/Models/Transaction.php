<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'm_transaction';
    protected $fillable = [
        "mt_payment_id", 
        "mt_loc_id", 
        "mt_organization_id", 
        "mt_amount", 
        "mt_cash_amount", 
        "mt_cash_change",
        "mt_discount",
        "mt_additional_field",
        "mt_state",
        "mt_code",
        "mt_order",
        "mt_payment_type_name",
        "mt_current_location"
    ];
    protected $primaryKey = "mt_id";
}
