<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Awb extends Model
{
    protected $table = 'm_awb';
    protected $fillable = [
        "ma_mk_id", 
        "ma_url", 
        "ma_price"
    ];
    protected $primaryKey = "ma_id";
}
