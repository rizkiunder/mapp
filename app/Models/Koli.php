<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Koli extends Model
{
    protected $table = 'm_koli';
    protected $fillable = [
        "mk_id",
        "mk_formula_id", 
        "mk_mco_id", 
        "mk_length", 
        "mk_width", 
        "mk_height", 
        "mk_description",
        "mk_volume",
        "mk_weight",
        "mk_code"
    ];
    protected $primaryKey = "mk_id";
}
