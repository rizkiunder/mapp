<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Connote extends Model
{
    protected $table = 'm_connote';
    protected $fillable = [
        "mco_mt_id", 
        "mco_organization_id", 
        "mco_service", 
        "mco_service_price", 
        "mco_amount", 
        "mco_booking_code",
        "mco_order",
        "mco_state",
        "mco_state_id",
        "mco_zone_from",
        "mco_zone_to",
        "mco_total_package",
        "mco_surcharge_amount",
        "mco_sla_day",
        "mco_actual_weight",
        "mco_volume_weight",
        "mco_chargeable_weight",
        "mco_ml_id",
        "mco_msta_id",
        "mco_number"
    ];
    protected $primaryKey = "mco_id";
}
