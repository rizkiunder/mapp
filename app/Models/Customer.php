<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'm_customer';
    protected $fillable = [
        "mc_zone_id", 
        "mc_organization_id", 
        "mc_code", 
        "mc_loc_id", 
        "mc_name", 
        "mc_address",
        "mc_email",
        "mc_phone",
        "mc_address_detail",
        "mc_zip_code"
    ];
    protected $primaryKey = "mc_id";
}
