<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'm_location';
    protected $fillable = [
        "ml_name", 
        "ml_type", 
        "ml_code", 
        "ml_status"
    ];
    protected $primaryKey = "ml_id";
}
