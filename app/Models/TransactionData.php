<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionData extends Model
{
    protected $table = 'm_transaction_data';
    protected $fillable = [
        "mtd_mt_id", 
        "mtd_owner", 
        "mtd_origin", 
        "mtd_destination",
        "ma_sicepat"
    ];
    protected $primaryKey = "mtd_id";
}
