<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionNote extends Model
{
    protected $table = 'm_transaction_note';
    protected $fillable = [
        "mtn_mt_id", 
        "mtn_description"
    ];
    protected $primaryKey = "mtn_id";
}
