# MILEAPP BACKEND DEVELOPER TEST WITH LARAVEL LUMEN

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Installation

Install the dependencies and devDependencies and start the server.

```sh
cd MAPP
composer install
```

For serve development...

```sh
php -S localhost:8000 -t public
http://localhost:8000
```

## Database Setup

import your database with name db is mapp
```sh
MAPP/database/database.sql
```

## Set Env

import your database with name db is mapp
```sh
APP_NAME=Lumen
APP_ENV=local
APP_KEY=
APP_DEBUG=true
APP_URL=http://localhost
APP_TIMEZONE=UTC

LOG_CHANNEL=stack
LOG_SLACK_WEBHOOK_URL=

DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=mapp
DB_USERNAME=root
DB_PASSWORD=

CACHE_DRIVER=array
QUEUE_DRIVER=array
QUEUE_CONNECTION=sync

```
