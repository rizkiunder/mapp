<?php

class PackageTest extends TestCase
{
    /**
     * /Package [GET]
     */
    public function testShouldReturnAllPackage(){

        $this->get("package", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure([
            [
                'transaction_id',
                'customer_name',
                'transaction_amount'
            ]
        ]);
        
    }

    /**
     * /package/id [GET]
     */
    public function testShouldReturnPackageDetail(){
        $this->get("package/d0090c40-539f-479a-8274-899b9970bddc", []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                "transaction_id",
                "customer_name",
                "customer_code",
                "transaction_amount",
                "transaction_discount",
                "transaction_additional_field",
                "transaction_state",
                "transaction_payment_type",
                "transaction_code",
                "transaction_order",
                "location_id",
                "organization_id",
                "created_at",
                "updated_at",
                "transaction_payment_type_name",
                "transaction_cash_amount",
                "transaction_cash_change",
                "connote_id",
                "connote",
                "origin_data",
                "destination_data",
                "koli_data",
                "custom_field",
                "currentLocation"
            ]    
        );
        
    }

    /**
     * /package [POST]
     */
    public function testShouldCreatePackage(){

        $parameters = [
            "connote_id" => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
            "koli_chargeable_weight" => 1,
            "koli_length"  => 2,
            "koli_width"  => 0,
            "koli_height"  => 0,
            "koli_description" => "Bubble Wrap",
            "koli_formula_id" => null,
            "koli_volume" => 0,
            "koli_weight" => 9,
            "awb_sicepat" => null,
            "harga_barang" => 32000
        ];

        $this->post("package", $parameters, []);
        $this->seeStatusCode(201);
        $this->seeJsonStructure(
            [
                "mk_id",
                "mk_formula_id",
                "mk_mco_id",
                "mk_length",
                "mk_width",
                "mk_height",
                "mk_description",
                "mk_volume",
                "mk_weight",
                "mk_chargeable_weight",
                "mk_code"
            ]    
        );
        
    }
    
    /**
     * /package/id [PUT]
     */
    public function testShouldUpdatePackage(){

        $parameters = [
            "mk_formula_id" => 0,
            "mk_mco_id"  => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
            "mk_length"  => 0,
            "mk_width"  => 0,
            "mk_height"  => 0,
            "mk_description" => "Bubble Glues",
            "mk_volume" => 0,
            "mk_weight" => 0,
            "mk_chargeable_weight" => 9
        ];

        $this->put("package/e2cb6d86-0bb9-409b-a1f0-389ed4f2df2d", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                "mk_id",
                "mk_formula_id",
                "mk_mco_id",
                "mk_length",
                "mk_width",
                "mk_height",
                "mk_description",
                "mk_volume",
                "mk_weight",
                "mk_chargeable_weight",
                "mk_code",
                "created_at",
                "updated_at"
            ]    
        );
    }

    /**
     * /package/id [PATCH]
     */
    public function testShouldUpdatePatchPackage(){

        $parameters = [
            "mk_description" => "Bubble"
        ];

        $this->patch("package/e2cb6d86-0bb9-409b-a1f0-389ed4f2df2d", $parameters, []);
        $this->seeStatusCode(200);
        $this->seeJsonStructure(
            [
                "mk_id",
                "mk_formula_id",
                "mk_mco_id",
                "mk_length",
                "mk_width",
                "mk_height",
                "mk_description",
                "mk_volume",
                "mk_weight",
                "mk_chargeable_weight",
                "mk_code",
                "created_at",
                "updated_at"
            ]    
        );
    }

    /**
     * /package/id [DELETE]
     */
    public function testShouldDeletePackage(){
        
        $this->delete("package/r528TmIb-r8gY-Nyz6-0Bfc-12AL2GSTYjCH", [], []);
        $this->seeStatusCode(404);
        $this->seeJsonStructure([
                'message'
        ]);
    }

}