/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.38-MariaDB : Database - mapp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `authors` */

DROP TABLE IF EXISTS `authors`;

CREATE TABLE `authors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `github` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latest_article_published` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Table structure for table `m_awb` */

DROP TABLE IF EXISTS `m_awb`;

CREATE TABLE `m_awb` (
  `ma_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ma_mk_id` varchar(255) NOT NULL,
  `ma_url` text,
  `ma_price` int(11) DEFAULT NULL,
  `ma_sicepat` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ma_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `m_connote` */

DROP TABLE IF EXISTS `m_connote`;

CREATE TABLE `m_connote` (
  `mco_id` varchar(255) NOT NULL,
  `mco_mt_id` varchar(255) NOT NULL,
  `mco_ma_id` int(11) DEFAULT NULL,
  `mco_organization_id` int(11) DEFAULT NULL,
  `mco_state_id` int(11) NOT NULL,
  `mco_service` varchar(255) NOT NULL,
  `mco_service_price` int(11) DEFAULT NULL,
  `mco_amount` int(11) NOT NULL,
  `mco_code` varchar(255) DEFAULT NULL,
  `mco_booking_code` varchar(255) DEFAULT NULL,
  `mco_order` int(11) NOT NULL,
  `mco_zone_from` int(11) DEFAULT NULL,
  `mco_zone_to` int(11) DEFAULT NULL,
  `mco_total_package` int(11) DEFAULT NULL,
  `mco_surcharge_amount` int(11) DEFAULT NULL,
  `mco_sla_day` int(1) DEFAULT NULL,
  `mco_actual_weight` int(11) DEFAULT NULL,
  `mco_volume_weight` int(11) DEFAULT NULL,
  `mco_chargeable_weight` int(11) DEFAULT NULL,
  `mco_ml_id` varchar(255) DEFAULT NULL,
  `mco_msta_id` int(11) DEFAULT NULL,
  `mco_number` int(11) NOT NULL,
  `mco_pod` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mco_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `m_customer` */

DROP TABLE IF EXISTS `m_customer`;

CREATE TABLE `m_customer` (
  `mc_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mc_zone_id` int(11) NOT NULL,
  `mc_organization_id` int(11) NOT NULL,
  `mc_code` varchar(255) DEFAULT NULL,
  `mc_loc_id` varchar(24) NOT NULL,
  `mc_name` varchar(255) NOT NULL,
  `mc_address` text NOT NULL,
  `mc_email` varchar(255) DEFAULT NULL,
  `mc_phone` varchar(50) DEFAULT NULL,
  `mc_address_detail` text NOT NULL,
  `mc_zip_code` int(8) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`mc_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `m_customer_attribute` */

DROP TABLE IF EXISTS `m_customer_attribute`;

CREATE TABLE `m_customer_attribute` (
  `mca_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mca_mc_id` int(11) NOT NULL,
  `mca_ms_id` int(11) NOT NULL,
  `mca_top` int(11) NOT NULL,
  `mca_type` varchar(255) NOT NULL,
  PRIMARY KEY (`mca_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `m_koli` */

DROP TABLE IF EXISTS `m_koli`;

CREATE TABLE `m_koli` (
  `mk_id` varchar(255) NOT NULL,
  `mk_formula_id` int(11) DEFAULT NULL,
  `mk_mco_id` varchar(255) NOT NULL,
  `mk_length` int(11) DEFAULT NULL,
  `mk_width` int(11) DEFAULT NULL,
  `mk_height` int(11) DEFAULT NULL,
  `mk_description` text,
  `mk_volume` int(11) DEFAULT NULL,
  `mk_weight` int(11) DEFAULT NULL,
  `mk_chargeable_weight` int(11) DEFAULT NULL,
  `mk_code` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`mk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `m_koli_note` */

DROP TABLE IF EXISTS `m_koli_note`;

CREATE TABLE `m_koli_note` (
  `mkn_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mkn_mk_id` varchar(255) NOT NULL,
  `mkn_awb_id` int(11) DEFAULT NULL,
  `mkn_mk_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`mkn_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `m_location` */

DROP TABLE IF EXISTS `m_location`;

CREATE TABLE `m_location` (
  `ml_id` varchar(255) NOT NULL,
  `ml_name` varchar(255) NOT NULL,
  `ml_type` varchar(255) NOT NULL,
  `ml_code` varchar(255) DEFAULT NULL,
  `ml_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ml_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `m_organization` */

DROP TABLE IF EXISTS `m_organization`;

CREATE TABLE `m_organization` (
  `mo_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mo_name` varchar(255) NOT NULL,
  PRIMARY KEY (`mo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `m_payment` */

DROP TABLE IF EXISTS `m_payment`;

CREATE TABLE `m_payment` (
  `mp_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mp_name` varchar(255) NOT NULL,
  PRIMARY KEY (`mp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

/*Table structure for table `m_sales` */

DROP TABLE IF EXISTS `m_sales`;

CREATE TABLE `m_sales` (
  `ms_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ms_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`ms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `m_source_tarif` */

DROP TABLE IF EXISTS `m_source_tarif`;

CREATE TABLE `m_source_tarif` (
  `msta_id` int(11) unsigned NOT NULL,
  `msta_name` varchar(255) NOT NULL,
  PRIMARY KEY (`msta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `m_state` */

DROP TABLE IF EXISTS `m_state`;

CREATE TABLE `m_state` (
  `mst_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mst_name` varchar(255) NOT NULL,
  PRIMARY KEY (`mst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `m_transaction` */

DROP TABLE IF EXISTS `m_transaction`;

CREATE TABLE `m_transaction` (
  `mt_id` varchar(255) NOT NULL,
  `mt_payment_id` int(11) NOT NULL,
  `mt_loc_id` varchar(255) NOT NULL,
  `mt_organization_id` int(11) NOT NULL,
  `mt_amount` int(11) DEFAULT NULL,
  `mt_cash_amount` int(11) NOT NULL,
  `mt_cash_change` int(11) NOT NULL,
  `mt_discount` int(11) DEFAULT NULL,
  `mt_additional_field` text,
  `mt_state` int(11) NOT NULL,
  `mt_code` varchar(255) NOT NULL,
  `mt_order` int(11) NOT NULL,
  `mt_payment_type_name` varchar(255) DEFAULT NULL,
  `mt_current_location` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`mt_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `m_transaction_data` */

DROP TABLE IF EXISTS `m_transaction_data`;

CREATE TABLE `m_transaction_data` (
  `mtd_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mtd_mt_id` varchar(255) NOT NULL,
  `mtd_owner` int(11) NOT NULL,
  `mtd_origin` int(11) NOT NULL,
  `mtd_destination` int(11) NOT NULL,
  PRIMARY KEY (`mtd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `m_transaction_note` */

DROP TABLE IF EXISTS `m_transaction_note`;

CREATE TABLE `m_transaction_note` (
  `mtn_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mtn_mt_id` varchar(255) NOT NULL,
  `mtn_description` text,
  PRIMARY KEY (`mtn_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Table structure for table `m_zone` */

DROP TABLE IF EXISTS `m_zone`;

CREATE TABLE `m_zone` (
  `mz_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mz_name` varchar(255) NOT NULL,
  `mz_code` varchar(255) NOT NULL,
  PRIMARY KEY (`mz_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
